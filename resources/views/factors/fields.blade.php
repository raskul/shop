<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Sum Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sum', 'Sum:') !!}
    {!! Form::text('sum', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Sum Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount_sum', 'Discount Sum:') !!}
    {!! Form::text('discount_sum', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Paid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_paid', 'Is Paid:') !!}
    {!! Form::text('is_paid', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('factors.index') !!}" class="btn btn-default">لغو</a>
</div>
