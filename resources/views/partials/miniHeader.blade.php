<style>
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;

    }

    li {
        float: left;
    }

    li a {
        display: block;
        color: black;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
    }

    li a:hover {
        color: gray;
    }

    .minimenu {
        padding-top: 30px;
        background-color: khaki;
        margin-bottom: 60px;
        height: 150px;
    }
</style>

<section class="minimenu">

    <div class="container">
        <div class="row">

            <div class="col-sm-4" style="color: #000;margin-top: 20px">
                @inject('header','App\classes\helpers\header')
                @if(Auth::check())
                    <div style="margin-bottom: 10px">
                        <a href="{{ route('user.profile') }}">
                            موجودی شما
                            ({{ $header->getUserBalance()}})
                            تومان
                        </a>
                    </div>
                    <div>
                        <a class=" dropdown-toggle" data-hover="dropdown"
                           href="{{ route('increase.balance.get') }}">افزایش موجودی<i
                                    class=""></i>
                        </a>
                    </div>
                @endif
            </div>

            <div class="col-sm-4 ">
                <div class="logo">
                    <span class="icon"><img alt="" src="{{ route('index.index') }}/images/logo.png"/></span>
                    <span class="text"><a href="{{ route('index.index') }}">فروشگاه <span>فایل</span></a></span>
                </div>
            </div>
            <div class="col-sm-4">
                <ul>
                    @if(! Auth::check())
                        <li><a href="{{ route('login.or.register') }}">ورود</a></li>
                        <li><a href="{{ route('login.or.register') }}">ثبت نام</a></li>
                    @else
                        <li>
                            <span><a href="#"
                                     onclick="event.preventDefault(); document.getElementById('logout-form').submit();">خروج</a>
                            <form id="logout-form" action="/logout" method="post"
                                  style="display: none">{{ csrf_field() }}</form></span>
                        </li>
                        <li><a href="{{ route('profile.get') }}">پروفایل</a></li>
                    @endif
                    <li><a href="contact.html">تماس با ما</a></li>
                </ul>

            </div>

        </div>
    </div>


</section>