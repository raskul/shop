@extends('layouts.index')
@section('header')
    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    {{--    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">--}}
    <link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
@endsection
@section('content')
    <div>
        @include('partials.header')
    </div>
    <div class="container" style="margin-top: 80px ">

        <div style="font-size: xx-large;padding-top: 30px;padding-bottom: 30px;color: #000;">{{$post->title}}</div>
        <div>{!! $post->text !!}</div>

    </div>
    <div style="height: 70px"></div>
    <div class="clearfix"></div>
    <div>
        @include('partials.footer')
    </div>
    <div class="clearfix"></div>
    <div>
        @include('partials.copyright')
    </div>
@endsection