@extends('layouts.app')
@push('header')
    <style>
        #user_name {
            float: right;
            background-color: #b4d6ff;
            border-radius: .4em;
            padding: 3px 10px 10px 10px;
        }

        #user_message {
            min-width: 100px;
            max-width: 543px;
            background-color: #b4d6ff;
            border-radius: .4em;
            margin: 25px 0px 0px 0px;
            padding: 5px;
            float: right;
        }

        #support_name {
            float: left;
            background-color: #bcff78;
            border-radius: .4em;
            padding: 3px 10px 10px 10px;
        }

        #support_message {
            min-width: 100px;
            min-height: 50px;
            max-width: 543px;
            background-color: #bcff78;
            border-radius: .4em;
            margin: 25px 0px 0px 0px;
            padding: 5px;
            float: left;
        }
    </style>
@endpush
@section('content')

    <div class="row" style="padding-top: 10px">
        <div class="col-sm-6 col-sm-offset-3">
            <div id="support_name">{!! $firstTicket->user->name !!}</div>
            <div id="support_message">{!! $firstTicket->text !!}</div>
            <div class="clearfix"></div>
            <hr>
        @if(isset($allTickets))
                @foreach($allTickets as $allTicket)
                    @php
                        if ($allTicket->user_id == Auth::user()->id){
                            $id_name = 'user_name';
                            $id_message = 'user_message';
                        }else{
                            $id_name = 'support_name';
                            $id_message = 'support_message';
                        }
                    @endphp
                    <div id="{{$id_name}}">{!! $allTicket->user->name !!} </div>
                    <div id="{{$id_message}}">
                        {!! $allTicket->text !!}
                        <br><span style="color: gray; font-size: xx-small">{{$allTicket->created_at}}</span>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                @endforeach
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            {!! Form::open(['route'=>['answer.ticket.post'], 'method' => 'post' ]) !!}

            <input type="hidden" name="parent_id" value="{{$parent_id}}">

            <!-- فیلد متن -->
            <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                {!! Form::label('text', 'متن') !!}
                {!! Form::textarea('text', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
                @if ($errors->has('text'))
                    <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                @endif
            </div>

            <button type="submit" class="btn btn-success">ارسال پاسخ</button>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
        });
    </script>
@endpush