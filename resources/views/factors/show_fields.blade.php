<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $factor->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $factor->user_id !!}</p>
</div>

<!-- Sum Field -->
<div class="form-group">
    {!! Form::label('sum', 'Sum:') !!}
    <p>{!! $factor->sum !!}</p>
</div>

<!-- Discount Sum Field -->
<div class="form-group">
    {!! Form::label('discount_sum', 'Discount Sum:') !!}
    <p>{!! $factor->discount_sum !!}</p>
</div>

<!-- Is Paid Field -->
<div class="form-group">
    {!! Form::label('is_paid', 'Is Paid:') !!}
    <p>{!! $factor->is_paid !!}</p>
</div>

