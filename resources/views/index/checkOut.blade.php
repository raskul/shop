@extends('layouts.index')
@section('header')
    {{--    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">
    {{--    <link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
@endsection
@section('content')

    <div> فاکتور فروش:</div>
    <hr>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>عنوان</th>
            <th>قیمت</th>
            <th>تخفیف</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{ $product->title }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->discount }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <button class="cusmo-btn narrow pull-right" type="submit">
        مجموع
        :
        {{ $sum-$discount }}
        تومان
        <br>
        برای پرداخت کلیک کنید
    </button>

@endsection