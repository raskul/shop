<section class="section-head">

    <div class="container">
        <div class="row-fluid top-row">
            <div class="span4">
                <div class="top-menu left">
                    <ul class="inline">
                        <li><a href="{{ route('shopping.cart') }}">خرید و پرداخت</a></li>
                        @if(! Auth::check())
                        <li><a href="{{ route('login.or.register') }}">ورود</a></li>
                        <li><a href="{{ route('login.or.register') }}">ثبت نام</a></li>
                        @else
                        <li>
                            <span><a href="#"
                                     onclick="event.preventDefault(); document.getElementById('logout-form').submit();">خروج</a>
                            <form id="logout-form" action="/logout" method="post"
                                  style="display: none">{{ csrf_field() }}</form></span>
                        </li>
                        <li><a href="{{ route('profile.get') }}">پروفایل</a></li>
                        @endif
                        <li><a href="contact.html">تماس با ما</a></li>
                    </ul>
                </div>
            </div>
            <div class="span4 logo-holder">
                <div class="logo">
                    <span class="icon"><img alt="" src="{{ route('index.index') }}/images/logo.png"/></span>
                    <span class="text"><a href="{{ route('index.index') }}">فروشگاه <span>فایل</span></a></span>
                </div>
            </div>
            <div class="span4 cart-holder">
                <div class="top-menu cart-menu">
                    <ul class="inline">
                        @inject('header','App\classes\helpers\header')
                        @if(Auth::check())
                        <li>
                            <a href="{{ route('user.profile') }}">
                                موجودی شما
                                ({{ $header->getUserBalance()}})
                                تومان

                            </a>
                        </li>
                        <li>
                            <div class="doviz-dropdown">
                                <a class=" dropdown-toggle" data-hover="dropdown"
                                   href="{{ route('increase.balance.get') }}">افزایش موجودی<i
                                            class=""></i>
                                </a>

                            </div>
                        </li>
                        @endif
                        <li>


                            @include('partials.basketItems')


                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="top-categories">
        <div class="container">
            <div class="row-fluid">
                <div class="span9">
                    <ul class="inline top-cat-menu">
                        @foreach($header->getCategories() as $category)
                            <li
                                    @if(isset($c))
                                        @if($c == $category->name)
                                            class="active"
                                        @endif
                                    @endif
                            >
                                <a href="
                                @if($category->name == 'همه محصولات')
                                    {{ route('index.index') }}
                                @else
                                    {{ route('filter', $category->name) }}
                                @endif
                                        ">{{ $category->name }}</a>
                        </li>
                        @endforeach
                    </ul>


                    <select class="top-cat-menu dropdown">
                        <option value="products-grid.html">
                            FACE
                        </option>


                        <option value="products-grid.html">
                            BODY
                        </option>
                        <option value="products-grid.html">
                            MAKE UP
                        </option>
                        <option value="products-grid.html">
                            HAIRS
                        </option>
                        <option value="products-grid.html">
                            PERFUMES
                        </option>
                        <option value="products-grid.html">
                            GIFTS
                        </option>
                        <option value="products-grid.html">
                            BRANDS
                        </option>

                        <option value="products-grid.html">
                            MUST HAVE
                        </option>


                    </select>


                </div>
                <div class="span3">
                    <div class="search-field-holder">
                        <form>
                            <input class="span12" type="text" placeholder="جست و جو">
                            <i class="icon-search"></i>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>