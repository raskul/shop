<!-- فیلد دسته بندی: -->
<div class="form-group col-sm-6 {{ $errors->has('category_id[]') ? ' has-error' : '' }}">
    {!! Form::label('category_id[]', 'دسته بندی:') !!}
    {!! Form::select('category_id[]', $categories, (isset($product))?$product->categories:null, ['class' => 'form-control select2']) !!}
    @if ($errors->has('category_id[]'))
        <span class="help-block"><strong>{{ $errors->first('category_id[]') }}</strong></span>
    @endif
</div>

<!-- فیلد دسته بندی: -->
<div class="form-group col-sm-6 {{ $errors->has('tag_id[]') ? ' has-error' : '' }}">
    {!! Form::label('tag_id[]', 'برچسب ها:') !!}
    {!! Form::select('tag_id[]', $tags, (isset($product))?$product->tags:null, ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
    @if ($errors->has('tag_id[]'))
        <span class="help-block"><strong>{{ $errors->first('tag_id[]') }}</strong></span>
    @endif
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'عنوان:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description_mini', 'توضیحات مختصر:') !!}
    {!! Form::text('description_mini', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'توضیحات:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description_full', 'توضیحات کامل:') !!}
    {!! Form::textarea('description_full', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
</div>

<!-- Download Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('download_link', 'لینک دانلود:') !!}
    {!! Form::text('download_link', null, ['class' => 'form-control']) !!}
</div>

<!-- Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('count', 'تعداد موجود در انبار:') !!}
    {!! Form::text('count', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'قیمت:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount', 'تخفیف:') !!}
    {!! Form::text('discount', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Main Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_main', 'تصویر اصلی:') !!}
    {!! Form::text('image_main', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Two Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_two', 'تصویر دوم:') !!}
    {!! Form::text('image_two', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Three Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_three', 'تصویر سوم:') !!}
    {!! Form::text('image_three', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Four Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_four', 'تصویر چهارم:') !!}
    {!! Form::text('image_four', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Five Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_five', 'تصویر پنجم:') !!}
    {!! Form::text('image_five', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sold_count', 'تعداد فروش رفته:') !!}
    {!! Form::text('sold_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('brand', 'برند:') !!}
    {!! Form::text('brand', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', 'مدت زمان:') !!}
    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vote', 'امتیاز:') !!}
    {!! Form::text('vote', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_voters', 'تعداد رای دهندگان:') !!}
    {!! Form::text('total_voters', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('products.index') !!}" class="btn btn-default">لغو</a>
</div>


