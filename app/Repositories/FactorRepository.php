<?php

namespace App\Repositories;

use App\Models\Factor;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FactorRepository
 * @package App\Repositories
 * @version January 15, 2018, 6:09 pm UTC
 *
 * @method Factor findWithoutFail($id, $columns = ['*'])
 * @method Factor find($id, $columns = ['*'])
 * @method Factor first($columns = ['*'])
*/
class FactorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'sum',
        'discount_sum',
        'is_paid'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Factor::class;
    }
}
