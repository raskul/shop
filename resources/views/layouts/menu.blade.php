<li class="{{ Request::is('upload') ? 'active' : '' }}" style="border-bottom: white solid 1px">
    <a href="{!! route('upload.get') !!}"><i class="fa fa-edit"></i><span>آپلود</span></a>
</li>


<li class="{{ Request::is('products*') ? 'active' : '' }}">
    <a href="{!! route('products.index') !!}"><i class="fa fa-edit"></i><span>محصولات</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>کاربران</span></a>
</li>

<li class="{{ Request::is('factors*') ? 'active' : '' }}">
    <a href="{!! route('factors.index') !!}"><i class="fa fa-edit"></i><span>فاکتور ها</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>دسته بندی ها</span></a>
</li>

<li class="{{ Request::is('tags*') ? 'active' : '' }}">
    <a href="{!! route('tags.index') !!}"><i class="fa fa-edit"></i><span>برچسب ها</span></a>
</li>

<li class="{{ Request::is('comments*') ? 'active' : '' }}">
    <a href="{!! route('comments.index') !!}"><i class="fa fa-edit"></i><span>دیدگاه ها</span></a>
</li>

<li class="{{ Request::is('posts*') ? 'active' : '' }}">
    <a href="{!! route('posts.index') !!}"><i class="fa fa-edit"></i><span>پست ها</span></a>
</li>

<li class="{{ Request::is('tickets*') ? 'active' : '' }}">
    <a href="{!! route('tickets.index') !!}"><i class="fa fa-edit"></i><span>تیکت ها</span></a>
</li>

