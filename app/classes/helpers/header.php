<?PHP
namespace App\classes\helpers;

use App\Models\Category;

class Header{
    function getUserBalance(){
        $balance = 0;
        if (\Auth::check()){
            $balance = auth()->user()->balance;
        }
        return $balance;
    }

    function getCategories(){
        return Category::get();
    }
}