@extends('layouts.index')
@section('header')
    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    {{--<link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">--}}
    <link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
@endsection
@section('content')

    <section class="section-checkout">
        <div class="container">

            <div class="phase-title current">
                <h1>برای خرید ابتدا ثبت نام کنید یا وارد شوید.</h1>
            </div>

            <div class="row-fluid">
                <div class="span6">
                    <div class="form-holder right-border">
                        <h4>ورود</h4>
                        <p>
                            در صورتی که در این وبسایت قبلا ثبت نام کرده اید یوزرنیم و پسورد خود را وارد کنید.
                        </p>
                        {!! Form::open(['route'=>['login' ], 'method' => 'post' ]) !!}
                        <div class="control-group">

                            <div class="controls">
                                <div class="form-label ">ایمیل</div>

                                <input type="text" id="email" name="email" class="required span12 cusmo-input"/>

                            </div>
                        </div>

                        <div class="control-group">

                            <div class="controls">
                                <div class="form-label ">رمز عبور</div>

                                <input id="password" type="password" name="password"
                                       class="required span12 cusmo-input"/>

                            </div>
                        </div>
                        <div class="rememberme">
                            <input type="checkbox" class="iCheck"> مرا به خاطر بسپار
                        </div>
                        <div class="forget-password">
                            <a href="#">رمز خود را فراموش کرده اید؟</a>
                        </div>
                        <div class="button-holder">
                            <button class="cusmo-btn narrow " type="submit">ورود</button>
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
                <div class="span6">
                    <div class="form-holder">
                        <h4>ثبت نام</h4>
                        <p>
                            همین حالا ثبت نام کنید.ثبت نام شما کمتر از 30 ثانیه وقت شما را میگیرد.
                        </p>
                        {!! Form::open(['route'=>['register' ], 'method' => 'post' ]) !!}
                        <div class="control-group">

                            <div class="controls">
                                <div class="form-label ">نام</div>

                                <input type="text" name="name" class="required span12 cusmo-input"/>

                            </div>

                            <div class="controls">
                                <div class="form-label ">ایمیل</div>

                                <input type="text" name="email" class="required span12 cusmo-input"/>

                            </div>
                        </div>

                        <div class="control-group">

                            <div class="controls">
                                <div class="form-label ">رمز عبور</div>

                                <input type="password" name="password" class="required span12 cusmo-input"/>

                            </div>
                        </div>
                        <div class="control-group">

                            <div class="controls">
                                <div class="form-label ">تکرار رمز عبور</div>

                                <input type="password" name="password_confirmation" class="required span12 cusmo-input"/>

                            </div>
                        </div>
                        <button class="cusmo-btn narrow pull-right" type="submit">ثبت نام</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="disabled-phases">
                <div class="phase-title">
                    <h1><a href="checkout-2.html">پرداخت</a></h1>
                </div>
            </div>
        </div>
    </section>

@endsection