@extends('layouts.index')
@section('header')
{{--    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">--}}
        <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">
{{--    <link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
@endsection
@section('content')

{{--    @include('partials.header')--}}
@include('partials.miniHeader')

    <section class="section-shopping-cart">
        <div class="container">
            <div class="row-fluid">

                <div class="span12">
                    <div class="page-content shopping-cart-page ">

                        @if($products != '')

                            <table class="table ">
                                <thead>
                                <tr>
                                    <th class="span2">تصویر</th>
                                    <th class="span5">نام محصول</th>
                                    {{--<th class="span2">تعداد</th>--}}
                                    <th class="span2 price-column" style="min-width: 100px">قیمت</th>
                                    <th class="span2 price-column" style="min-width: 100px">تخفیف</th>
                                    <th class="span1 price-column" style="min-width: 100px">جمع</th>
                                    <th class="span2">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($products as $product)
                                    <tr>
                                        <td>
                                            <div class="thumb">
                                                <img alt="" src="{{ $product->image_main }}" width="100px" height="100px"/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="desc">
                                                <h3>{{ $product->title }}</h3>
                                                <div class="tag-line">
                                                    {{ $product->description }}
                                                </div>
                                                <div class="pid">کد محصول: {{ $product->id }}</div>
                                            </div>
                                        </td>
                                        {{--<td>--}}
                                        {{--<div class="quantity">--}}
                                        {{--<select class="chosen-select">--}}
                                        {{--<option value="1">1</option>--}}
                                        {{--<option value="2">2</option>--}}
                                        {{--<option value="3">3</option>--}}
                                        {{--<option value="4">4</option>--}}
                                        {{--<option value="5">5</option>--}}
                                        {{--<option value="6">6</option>--}}
                                        {{--<option value="7">7</option>--}}
                                        {{--<option value="8">8</option>--}}
                                        {{--<option value="9">9</option>--}}

                                        {{--</select>--}}
                                        {{--</div>--}}
                                        {{--</td>--}}
                                        <td>
                                            <div class="price">
                                                {{ $product->price }}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="price">
                                                {{ $product->discount }}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="price">
                                                {{ ($product->price)-($product->discount) }}
                                            </div>
                                        </td>
                                        {!! Form::open(['route'=>['remove.from.basket', $product->id ], 'method' => 'post' ]) !!}
                                        <td>
                                            <div>
                                                {{--<div class="delete">--}}
                                                {{--class="close-btn"--}}
                                                <button type="submit" class="btn-danger btn">حذف</button>
                                            </div>
                                        </td>
                                        {!! Form::close() !!}
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                            <div class="buttons-holder">
                                <a class="cusmo-btn gray narrow" href="{{ route('index.index') }}">اضافه کردن محصولات
                                    دیگر</a>
                                <a class="cusmo-btn narrow" href="{{ route('check.out') }}">تکمیل فرایند خرید</a>
                            </div>


                        @else
                            <div> هیچ محصولی در سبد شما وجود ندارد.</div>
                            <div class="buttons-holder pull-right">
                                <a class="cusmo-btn gray narrow" href="{{ route('index.index') }}">بازگشت</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--@include('partials.footer')--}}
@include('partials.miniFooter')

@endsection