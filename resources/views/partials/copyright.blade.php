<section class="section-copyright">
    <div class="container">
        <div class="copyright pull-left">
            <p>
                <strong>© ss 2018</strong>. All rights reserved.<br>
                Designed by <a href="">ss</a> | Coded
                by <a href="#">ss</a>
            </p>
        </div>
        <div class="copyright-links pull-right">
            <ul class="inline">
                <li><a href="#">حریم شخصی و سیاست ها</a></li>
                <li><a href="#">قوانین و مقرات</a></li>
                <li><a href="#">نقشه سایت</a></li>
            </ul>
        </div>
    </div>
</section>

