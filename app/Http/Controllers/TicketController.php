<?php

namespace App\Http\Controllers;

use App\DataTables\TicketDataTable;
use App\Http\Requests\CreateTicketRequest;
use App\Http\Requests\UpdateTicketRequest;
use App\Models\Ticket;
use App\Repositories\TicketRepository;
use Auth;
use Flash;
use Illuminate\Http\Request;
use Response;

class TicketController extends AppBaseController
{
    /** @var  TicketRepository */
    private $ticketRepository;

    public function __construct(TicketRepository $ticketRepo)
    {
        $this->ticketRepository = $ticketRepo;
    }

    /**
     * Display a listing of the Ticket.
     *
     * @param TicketDataTable $ticketDataTable
     * @return Response
     */
    public function index(TicketDataTable $ticketDataTable)
    {
        return $ticketDataTable->render('tickets.index');
    }

    /**
     * Show the form for creating a new Ticket.
     *
     * @return Response
     */
    public function create()
    {
        return view('tickets.create');
    }

    /**
     * Store a newly created Ticket in storage.
     *
     * @param CreateTicketRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketRequest $request)
    {
        if (Auth::check()) {
            $input['user_id'] = auth()->user()->id;
            $input['email'] = auth()->user()->email;
        } else {
            $input['user_id'] = $request->user_id;
            $input['email'] = $request->email;
        }
        $input['text'] = $request->text;
        $input['is_read'] = 0;
        $input['who_answer'] = null;


        $ticket = $this->ticketRepository->create($input);

        Flash::success('Ticket saved successfully.');

        return redirect(route('tickets.index'));
    }

    /**
     * Display the specified Ticket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        return view('tickets.show')->with('ticket', $ticket);
    }

    /**
     * Show the form for editing the specified Ticket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        return view('tickets.edit')->with('ticket', $ticket);
    }

    /**
     * Update the specified Ticket in storage.
     *
     * @param  int              $id
     * @param UpdateTicketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketRequest $request)
    {
        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

//        if (Auth::check()) {
//            $input['user_id'] = auth()->user()->id;
//            $input['email'] = auth()->user()->email;
//        } else {
        $input['user_id'] = $request->user_id;
        $input['email'] = $request->email;
//        }
        $input['text'] = $request->text;
        $input['is_read'] = 0;
        $input['who_answer'] = $request->who_answer;

        $ticket = $this->ticketRepository->update($input, $id);

        Flash::success('Ticket updated successfully.');

        return redirect(route('tickets.index'));
    }

    /**
     * Remove the specified Ticket from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        $this->ticketRepository->delete($id);

        Flash::success('Ticket deleted successfully.');

        return redirect(route('tickets.index'));
    }

    public function answerTicket(Ticket $ticket)
    {
        $parent_id = $ticket->parent_id;
        if ($parent_id != null) {
            $allTickets = Ticket::where('parent_id', $parent_id)->orderBy('created_at')->get();
        } else {
            $allTickets = Ticket::where('parent_id', $ticket->id)->orderBy('created_at')->get();
        }



        if ($parent_id == null) {
            $firstTicket = $ticket;
            $parent_id = $ticket->id;
        }else{
            $firstTicket = Ticket::where('id', $parent_id)->first();
        }


        return view('tickets.answerToTicket', compact('parent_id', 'firstTicket', 'allTickets'));
    }

    public function answerTicketPost(Request $request)
    {
        $inputs['user_id'] = auth()->user()->id;
        $inputs['email'] = auth()->user()->email;
        $inputs['text'] = $request->text;
        $inputs['closed'] = 1;
        $inputs['is_read'] = 1;
        $inputs['parent_id'] = $request->parent_id;
        $inputs['who_answer'] = auth()->user()->name;

        Ticket::create($inputs);
        Ticket::where('parent_id', $inputs['parent_id'])->orWhere('id', $inputs['parent_id'])->update(['closed' => 1, 'is_read' => 1]);

        return redirect(route('tickets.index'));
    }
}
