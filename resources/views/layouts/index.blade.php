<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>فروشگاه فایل</title>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,700,600,800' rel='stylesheet' type='text/css'>

    @yield('header')



    <link href="{{asset('css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/flexslider.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('iransans/css/fontiran.css') }}">

    <!--[if IE 7]>
    <link href="{{asset('css/font-awesome/css/font-awesome-ie7.min.css')}}" rel="stylesheet"><![endif]-->

    <link rel="stylesheet" href="{{asset('css/style-rtl.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">



    @stack('header')

    <style>
        body {
            font-family: IRANSans !important;
            font-weight: 300;
            direction: rtl;
            /*background-color: #E2E2E2;*/
            margin: 0;
        }

        h1, h2, h3, h4, h5, h6, input, textarea, div, a, li, ul, button, p, label, span {
            font-family: IRANSans !important;
        }

        .datepicker-plot-area {
            position: relative;
        }

        .cusmo-btn {
            font-family: IRANSans !important;
        }
    </style>

</head>

<body >

@yield('content')

<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.1.1.min.js"></script>
<script src="{{asset('css/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/css_browser_selector.js')}}"></script>
<script type="text/javascript" src="{{asset('js/twitter-bootstrap-hover-dropdown.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.easing-1.3.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.flexslider-min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/script.js')}}"></script>

@yield('scripts')
@stack('scripts')
</body>
</html>