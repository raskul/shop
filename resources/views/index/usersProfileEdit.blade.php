@extends('layouts.index')
@section('header')
    {{--    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">
    {{--    <link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
@endsection
@push('header')
    <style>
        label{
            margin-top: 10px;
        }
    </style>
@endpush
@section('content')
    @include('partials.miniHeader')

    <div class="row">
        <div class="col-sm-3 col-sm-offset-3">

            {!! Form::open(['route'=>['profile.post', $user->id ] ]) !!}

            <label for="name">نام:</label>
            <input id="name" type="text" name="name" class="form-control" value="{{ $user->name }}">

            <label for="email">ایمیل:</label>
            <input id="email" type="text" name="email" class="form-control" value="{{ $user->email }}">

            <label for="password">رمز عبور:</label>
            <input id="password" type="password" name="password" class="form-control">

            <label for="password_retype">تکرار رمز عبور:</label>
            <input id="password_retype" type="password" name="password_retype" class="form-control" >
            <br>
            <button type="submit" class="btn btn-success">ذخیره</button>
            <br>
            <br>
            <span style="color: red">*</span>
            <span>درصورتی که نمیخواید رمز عبور خود را تغییر دهید قسمت رمز عبور را خالی بگذارید.</span>

            {!! Form::close() !!}

        </div>
    </div>

    <div style="height: 70px"></div>
    <div>@include('partials.miniFooter')</div>

@endsection