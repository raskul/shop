<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شماره:') !!}
    <p>{!! $category->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'نام:') !!}
    <p>{!! $category->name !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('parent_id', 'نام والد:') !!}
    <p>{!! $category->parent_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاریخ ساخت') !!}
    <p>{!! $category->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'تاریخ بروزرسانی') !!}
    <p>{!! $category->updated_at !!}</p>
</div>

