<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Category
 * @package App\Models
 * @version January 15, 2018, 6:08 pm UTC
 *
 * @property string name
 */
class Category extends Model
{

    public $table = 'categories';
    


    public $fillable = [
        'name', 'parent_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    
}
