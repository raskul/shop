
@if(Session::has('itemId'))
    @php
        $id = session()->get('itemId');
        $whereData = [ 'myselect' => $id ];
        $query = '';
        foreach ($whereData['myselect'] as $select){
            $query .= " id = $select or";
        }
        $query = 'select * from products where'. $query . ' id = 0' ;
        $products = DB::select(DB::raw($query));
    @endphp
@endif
<div class="basket">
    <div class="basket-item-count">
        {{--basket items count start--}}
        @if(Session::has('itemId'))
        <span style="color: red">{{ count($products) }}</span>
        @else
        0
        @endif
        {{--basket items count end--}}
    </div>
    <div class="total-price-basket">
        {{--basket items sum price start--}}
        @if(Session::has('itemId'))
            @php
            $sum = 0;
                foreach($products as $product){
                $sum += $product->price ;
                }
            @endphp
            <span>{{ $sum }}</span>
        @else
        0
        @endif
        <span style="font-size: x-small"> تومان </span>

        {{--basket item sum price end--}}
    </div>
    <div class="dropdown">
        <a class="dropdown-toggle" data-hover="dropdown" href="{{ route('shopping.cart') }}">
            <img alt="basket" src="{{ route('index.index') }}/images/icon-basket.png"/>
        </a>
        {{--basket-start--}}

        <ul class="dropdown-menu">
            {{--basket item start--}}
            @if(Session::has('itemId'))
                @foreach($products as $product)
                    <li>
                        <div class="basket-item">
                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="thumb">
                                        <img alt="" src="{{ $product->image_main }}" width="50px" height="50px"/>
                                    </div>
                                </div>
                                <div class="span8">
                                    <div class="title">{{ $product->title }}</div>
                                    <div class="price">{{ $product->price }}</div>
                                </div>
                            </div>
                            {!! Form::open(['route'=>['remove.from.basket', $product->id ], 'method' => 'post' ]) !!}
                            <button class="close-btn" type="submit"></button>
                            {!! Form::close() !!}
                        </div>
                    </li>
                @endforeach
            {{--basket item end--}}
            @endif

            {{--<li>--}}
            {{--<div class="basket-item">--}}
            {{--<div class="row-fluid">--}}
            {{--<div class="span4">--}}
            {{--<div class="thumb">--}}
            {{--<img alt="" src="images/p2.jpg"/>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="span8">--}}
            {{--<div class="title">Versache</div>--}}
            {{--<div class="price">$120.00</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<a class="close-btn" href="#"></a>--}}
            {{--</div>--}}
            {{--</li>--}}

            {{--<li>--}}
            {{--<div class="basket-item">--}}
            {{--<div class="row-fluid">--}}
            {{--<div class="span4">--}}
            {{--<div class="thumb">--}}
            {{--<img alt="" src="images/p3.jpg"/>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="span8">--}}
            {{--<div class="title">Dark Shadow</div>--}}
            {{--<div class="price">$99.99</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<a class="close-btn" href="#"></a>--}}
            {{--</div>--}}
            {{--</li>--}}

            <li class="checkout">
                <a href="{{ route('shopping.cart') }}" class="cusmo-btn">تکمیل خرید</a>
            </li>
        </ul>

    </div>

</div>