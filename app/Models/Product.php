<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Product
 * @package App\Models
 * @version January 15, 2018, 6:07 pm UTC
 *
 * @property string title
 * @property string description
 * @property string download_link
 * @property integer count
 * @property integer price
 * @property integer discount
 * @property string image_main
 * @property string image_two
 * @property string image_three
 * @property string image_four
 * @property string image_five
 */
class Product extends Model
{

    public $table = 'products';
    


    public $fillable = [
        'title',
        'description_mini',
        'description',
        'description_full',
        'specification',
        'download_link',
        'count',
        'price',
        'real_price',
        'discount',
        'sold_count',
        'brand',
        'duration',
        'vote',
        'total_voters',
        'is_active',
        'image_main',
        'image_two',
        'image_three',
        'image_four',
        'image_five',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'download_link' => 'string',
        'count' => 'integer',
        'price' => 'integer',
        'discount' => 'integer',
        'image_main' => 'string',
        'image_two' => 'string',
        'image_three' => 'string',
        'image_four' => 'string',
        'image_five' => 'string',
        'image_five' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required'
    ];

    public function factors()
    {
        return $this->belongsToMany(Factor::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    
}
