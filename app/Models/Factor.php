<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Factor
 * @package App\Models
 * @version January 15, 2018, 6:09 pm UTC
 *
 * @property integer user_id
 * @property integer sum
 * @property integer discount_sum
 * @property integer is_paid
 */
class Factor extends Model
{

    public $table = 'factors';
    


    public $fillable = [
        'user_id',
        'sum',
        'discount_sum',
        'is_paid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'sum' => 'integer',
        'discount_sum' => 'integer',
        'is_paid' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
}
