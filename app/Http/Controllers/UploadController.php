<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadController extends Controller
{
    public $path = 'uploads/files/';

    public function uploadGet()
    {
        return view('index.upload');
    }

    public function uploadPost(Request $request)
    {
//        $this->authorize('is-writer');

        $username = auth()->user()->name;
        $fullPath = $this->path . $username . '/';
        $time = time();

        if (isset($request->file1)){
            $inputs['name1'] = $time . $request->file('file1')->getClientOriginalName();
            $request->file1->move($fullPath, $inputs['name1']);
            $url1 = route('index.index') . '/' . $fullPath . $inputs['name1'];
        }
        if (isset($request->file2)){
            $inputs['name2'] = $time . $request->file('file2')->getClientOriginalName();
            $request->file2->move($fullPath, $inputs['name2']);
            $url2 = route('index.index') . '/' . $fullPath . $inputs['name2'];
        }
        if (isset($request->file3)){
            $inputs['name3'] = $time . $request->file('file3')->getClientOriginalName();
            $request->file3->move($fullPath, $inputs['name3']);
            $url3 = route('index.index') . '/' . $fullPath . $inputs['name3'];
        }
        if (isset($request->file4)){
            $inputs['name4'] = $time . $request->file('file4')->getClientOriginalName();
            $request->file4->move($fullPath, $inputs['name4']);
            $url4 = route('index.index') . '/' . $fullPath . $inputs['name4'];
        }
        if (isset($request->file5)){
            $inputs['name5'] = $time . $request->file('file5')->getClientOriginalName();
            $request->file5->move($fullPath, $inputs['name5']);
            $url5 = route('index.index') . '/' . $fullPath . $inputs['name5'];
        }

        return view('index.upload', compact('url1', 'url2', 'url3', 'url4', 'url5'));

    }
}
