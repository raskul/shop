@extends('layouts.index')
@section('header')
    {{--    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">
    {{--    <link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
    <style>
        #user_name {
            float: right;
            background-color: #b4d6ff;
            border-radius: .4em;
            padding: 3px 10px 10px 10px;
        }

        #user_message {
            min-width: 100px;
            max-width: 543px;
            background-color: #b4d6ff;
            border-radius: .4em;
            margin: 25px 0px 0px 0px;
            padding: 5px;
            float: right;
        }

        #support_name {
            float: left;
            background-color: #bcff78;
            border-radius: .4em;
            padding: 3px 10px 10px 10px;
        }

        #support_message {
            min-width: 100px;
            min-height: 50px;
            max-width: 543px;
            background-color: #bcff78;
            border-radius: .4em;
            margin: 25px 0px 0px 0px;
            padding: 5px;
            float: left;
        }
    </style>
@endsection
@section('content')
    <div>
        @include('partials.miniHeader')
    </div>
    <div class="container" style="margin-top: 80px;max-width: 700px ">

        @if(isset($tickets))
            <div id="user_name">{!! $firstTicket->user->name !!}</div>
            <div id="user_message">{!! $firstTicket->text !!}</div>

            <div class="clearfix"></div>
            @foreach($tickets as $ticket)
                @php
                    if ($ticket->user_id == Auth::user()->id){
                        $id_name = 'user_name';
                        $id_message = 'user_message';
                    }else{
                        $id_name = 'support_name';
                        $id_message = 'support_message';
                    }
                @endphp
                <div id="{{$id_name}}">{!! $ticket->user->name !!} </div>
                <div id="{{$id_message}}">
                    {!! $ticket->text !!}
                    <br><span style="color: gray; font-size: xx-small">{{$ticket->created_at}}</span>
                </div>
                <div class="clearfix"></div>
                <hr>
            @endforeach
        @endif

        <div id="myDiv"></div>

        @include('flash::message')

        {!! Form::open(['route'=>['ticket.post'], 'method' => 'post' ]) !!}

        <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
            {!! Form::label('text', 'متن پیغام:') !!}
            {!! Form::textarea('text', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
            @if ($errors->has('text'))
                <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
            @endif
        </div>

        <button class="btn btn-success">ارسال پیغام</button>

        {!! Form::close() !!}

    </div>
    <div class="clearfix"></div>

    <div>@include('partials.miniFooter')</div>

@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('editor/basic/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/basic/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
            $('html, body').animate({
                scrollTop: $("#myDiv").offset().top
            }, 2000);
        });
    </script>
@endpush