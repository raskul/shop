@extends('layouts.index')
@section('header')
    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    {{--    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">--}}
    <link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">

    <style>
        @media screen and (max-width: 770px) {
            .mobile-hidden {
                visibility: hidden;
                clear: both;
                float: left;
                margin: 10px auto 5px 20px;
                width: 28%;
                display: none;
            }
        }
    </style>
@endsection
@section('content')

    @include('partials.header')

<section class="section-two-columns">
    <div class="container">
        <div class="row-fluid">
            <div class="span3 ">
                <div class="sidebar ">
                    <div class="accordion-widget category-accordions mobile-hidden">

                        <div class="accordion">
                            @for($z = 0 ; $z < 9 ; $z++)
                                @php
                                    $rand = rand(0 , 9);
                                @endphp
                                <div class="" style="border-bottom: 1px rgba(128,128,128,0.13) solid;border-top: 1px rgba(128,128,128,0.13) solid">
                                    <div class="row">
                                        <a href="{{ route('show.product.page', $products[$rand]->id) }}">
                                        <div class="col-sm-3"><img src="{{ $products[$rand]->image_main }}" alt=""
                                                                   width="100%" height="100%"></div>
                                        <div class="col-sm-6">{{ $products[$rand]->title }} </div>
                                        </a>
                                    </div>
                                </div>
                            @endfor
                        </div>

                    </div>
                </div>
            </div>
            <div class="span9 ">
                <div class="page-content">
                    <div class="products-page-head">
                        <h1>{{ $product->title }}</h1>
                        <div class="tag-line">
                            <span> {{ $product->description_mini }} </span>
                        </div>
                    </div>
                    <div class="row-fluid">

                        <div class="span7">
                            <div class="flexslider product-gallery">
                                <ul class="slides" style="float: left;">
                                    @if(isset($product->image_main))
                                        <li data-thumb="{{ $product->image_main }}">
                                            <img alt="" src="{{ $product->image_main }}"/>
                                    </li>
                                    @endif
                                    @if(isset($product->image_two))
                                        <li data-thumb="{{ $product->image_two }}">
                                            <img alt="" src="{{ $product->image_two }}"/>
                                    </li>
                                    @endif
                                    @if(isset($product->image_three))
                                        <li data-thumb="{{ $product->image_three }}">
                                            <img alt="" src="{{ $product->image_three }}"/>
                                    </li>
                                    @endif
                                    @if(isset($product->image_four))
                                        <li data-thumb="{{ $product->image_four }}">
                                            <img alt="" src="{{ $product->image_four }}"/>
                                    </li>
                                    @endif
                                    @if(isset($product->image_five))
                                        <li data-thumb="{{ $product->image_five }}">
                                            <img alt="" src="{{ $product->image_five }}"/>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                        </div>

                        <div class="span5">
                            <div class="product-info-box">
                                <div class="star-holder">
                                    <strong>امتیاز</strong>
                                    <div class="star" data-score="{{ $product->vote }}"></div>

                                    <div class="review-counter">
                                        @if(isset($comments))
                                            {{ count($comments) }}
                                            دیدگاه
                                        @else
                                            بدون دیدگاه
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="info-holder">
                                    <h4> کد محصول: {{ $product->id }}</h4>
                                    <p>
                                        {{ $product->description }}
                                    </p>
                                </div>
                                <hr>
                                <div class="price-holder">
                                    <div class="price">
                                        <span>{{ $product->price-$product->discount }}
                                            <span> تومان </span>
                                        </span>
                                        <span class="old">{{ $product->price.' تومان ' }}</span>
                                    </div>
                                </div>
                                <div class="buttons-holder">
                                    {!! Form::open(['route'=>['add.to.basket', $product->id ], 'method' => 'post' ]) !!}

                                    <button type="submit" class="cusmo-btn add-button">افزودن به سبد خرید</button>

                                    {!! Form::close() !!}

                                    <a class="add-to-wishlist-btn" href="#"><i class="icon-plus"></i>خرید سریع</a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="product-tabs">
                        <div class="controls-holder nav-tabs">
                            <ul class="inline">
                                <li class="active"><a data-toggle="tab" href="#description">توضیحات</a></li>
                                <li><a data-toggle="tab" href="#how-to-use">مشخصات فنی</a></li>
                                <li><a data-toggle="tab" href="#reviews">
                                        دیدگاه ها
                                        @if(isset($comments))
                                            ({{ count($comments) }})
                                        @endif
                                    </a></li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div id="description" class=" active tab-pane ">
                                @if(isset($product->description_full))
                                    {{ $product->description_full }}
                                @else
                                    <span>توضیحات کاملی برای این محصول ثبت نشده است.</span>
                                @endif
                            </div>

                            <div id="how-to-use" class=" tab-pane ">
                                @if(isset($product->specification))
                                    {{ $product->specification }}
                                @else
                                    <span>مشخصات فنی برای این محصول ثبت نشده است.</span>
                                @endif
                            </div>

                            <div id="reviews" class=" tab-pane ">
                                @if(Auth::check())
                                    {!! Form::open(['route'=>['comment.store.sec', 'product_id' => $product->id], 'method' => 'post' ]) !!}
                                    <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                                        {!! Form::textarea('text', null, ['class' => 'span12', 'id' => 'write-review-text', 'placeholder'=>"دیدگاه خود را اینجا بنویسید..."]) !!}
                                        @if ($errors->has('text'))
                                            <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                                        @endif
                                    </div>

                                    <div class="review-info">
                                        <div class="remaining-chars">
                                            <span id="counter">190</span> کاراکتر باقی مانده
                                        </div>
                                        <div class="star-holder">
                                            <strong>امتیاز</strong>
                                            <div class="star" data-score="{{ $product->vote }}"></div>
                                            <button type="submit" class="cusmo-btn">نوشتن دیدگاه</button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                @else
                                    <div>برای ثبت دیدگاه ابتدا
                                        <a href="{{ route('login.or.register') }}">
                                            <strong>
                                                وارد
                                            </strong>
                                        </a>
                                        اکانت خود شوید یا
                                        <a href="{{ route('login.or.register') }}">
                                            <strong>
                                                ثبت نام
                                            </strong>
                                        </a>
                                        کنید.
                                    </div>
                                @endif

                                <hr>

                                <div class="recent-reviews">

                                    @if(isset($comments))
                                        @foreach($comments as $comment)
                                            <div class="review-item">
                                                <div class="row-fluid">

                                                    <div class="span10">
                                                        <div class="body">
                                                            <h4> {{ $comment->user->name }}</h4>
                                                            <span class="date">{{ $comment->created_at }}</span>
                                                            <p>{{ $comment->text }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="span2">
                                                        <div class="thumb">
                                                            <img alt="avatar"
                                                                 src="{{ route('index.index') }}/images/default-avatar.png"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div>هیچ دیدگاهی برای این محصول وجود ندارد.</div>
                                    @endif

                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="middle-header-holder">
                        <div class="middle-header">محصولات دیگر</div>
                    </div>

                    <div class="products-holder related-products">
                        <div class="row-fluid">
                            @for( $y = 1; $y <= 3; $y++)
                                @php
                                    $rand = rand(0,9);
                                @endphp
                                <div class="span4">
                                    <div class="product-item">

                                        <img alt="" src="{{ $products[$rand]->image_main }}"/>
                                        <h1>{{ $products[$rand]->brand }}</h1>
                                        <div class="tag-line">
                                            <span>{{ $products[$rand]->title }}</span>
                                            <span>{{ $products[$rand]->description_mini }}</span>
                                        </div>
                                        @if($product->discount > 0)
                                            <div class="old">
                                                {{ $product->price . ' تومان ' }}
                                            </div>
                                        @endif
                                        <div class="price">
                                            {{ ($products[$rand]->price-$products[$rand]->discount) . ' تومان ' }}
                                        </div>
                                        {!! Form::open(['route'=>['add.to.basket', $product->id ], 'method' => 'post' ]) !!}

                                        <button type="submit" class="cusmo-btn add-button">افزودن به سبد خرید</button>

                                        {!! Form::close() !!}

                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    @include('partials.copyright')

@endsection
@push('scripts')
    <script type="text/javascript" src="{{asset('js/chosen.jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.raty.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/bootstrap-slider.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
@endpush