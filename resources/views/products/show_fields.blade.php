<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $product->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $product->title !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $product->description !!}</p>
</div>

<!-- Download Link Field -->
<div class="form-group">
    {!! Form::label('download_link', 'Download Link:') !!}
    <p>{!! $product->download_link !!}</p>
</div>

<!-- Count Field -->
<div class="form-group">
    {!! Form::label('count', 'Count:') !!}
    <p>{!! $product->count !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $product->price !!}</p>
</div>

<!-- Discount Field -->
<div class="form-group">
    {!! Form::label('discount', 'Discount:') !!}
    <p>{!! $product->discount !!}</p>
</div>

<!-- Image Main Field -->
<div class="form-group">
    {!! Form::label('image_main', 'Image Main:') !!}
    <p>{!! $product->image_main !!}</p>
</div>

<!-- Image Two Field -->
<div class="form-group">
    {!! Form::label('image_two', 'Image Two:') !!}
    <p>{!! $product->image_two !!}</p>
</div>

<!-- Image Three Field -->
<div class="form-group">
    {!! Form::label('image_three', 'Image Three:') !!}
    <p>{!! $product->image_three !!}</p>
</div>

<!-- Image Four Field -->
<div class="form-group">
    {!! Form::label('image_four', 'Image Four:') !!}
    <p>{!! $product->image_four !!}</p>
</div>

<!-- Image Five Field -->
<div class="form-group">
    {!! Form::label('image_five', 'Image Five:') !!}
    <p>{!! $product->image_five !!}</p>
</div>

<!-- Image Five Field -->
<div class="form-group">
    {!! Form::label('image_five', 'Image Five:') !!}
    <p>{!! $product->image_five !!}</p>
</div>

