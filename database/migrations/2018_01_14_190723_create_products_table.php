<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description_mini')->nullable();
            $table->text('description')->nullable();
            $table->text('description_full')->nullable();
            $table->text('specification')->nullable();
            $table->string('download_link')->nullable();
            $table->integer('count')->nullable()->default(1);
            $table->integer('price')->nullable()->default(0);
            $table->integer('real_price')->nullable()->default(0);
            $table->integer('discount')->nullable()->default(0);
            $table->integer('sold_count')->nullable()->default(0);
            $table->string('brand')->nullable();
            $table->string('duration')->nullable();
            $table->integer('vote')->nullable()->default(0);
            $table->integer('total_voters')->nullable()->defaulte(0);
            $table->string('image_main')->nullable();
            $table->string('image_two')->nullable();
            $table->string('image_three')->nullable();
            $table->string('image_four')->nullable();
            $table->string('image_five')->nullable();
            $table->integer('is_active')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
