<?php

namespace App\Http\Controllers;

use App\DataTables\ProductDataTable;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Tag;
use App\Repositories\ProductRepository;
use Flash;
use Response;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param ProductDataTable $productDataTable
     * @return Response
     */
    public function index(ProductDataTable $productDataTable)
    {
        return $productDataTable->render('products.index');
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        $tags = Tag::pluck('name', 'id');

        $categories = Category::pluck('name', 'id');

        return view('products.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
//        $input = $request->all();

        $input['title'] = $request->title;
        $input['description_mini'] = $request->description_mini;
        $input['description'] = $request->description;
        $input['description_full'] = $request->description_full;
        $input['specification'] = $request->specification;
        $input['download_link'] = $request->download_link;
        $input['count'] = $request->count;
        $input['price'] = $request->price;
        $input['discount'] = $request->discount;
        $input['sold_count'] = $request->sold_count;
        $input['brand'] = $request->brand;
        $input['duration'] = $request->duration;
        $input['vote'] = $request->vote;
        $input['total_voters'] = $request->total_voters;
        $input['image_main'] = $request->image_main;
        $input['image_two'] = $request->image_two;
        $input['image_three'] = $request->image_three;
        $input['image_four'] = $request->image_four;
        $input['image_five'] = $request->image_five;

        $input['real_price'] = $input['price'] - $input['discount'];

        if (isset($request->is_active))
            $input['is_active'] = $request->is_active;

        $product = $this->productRepository->create($input);

        $product->tags()->sync($request->tag_id);

        $product->categories()->sync($request->category_id);


        Flash::success('Product saved successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $tags = Tag::pluck('name', 'id');

        $categories = Category::pluck('name', 'id');

        return view('products.edit', compact('product', 'tags', 'categories'));
    }

    /**
     * Update the specified Product in storage.
     *
     * @param  int              $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $input['title'] = $request->title;
        $input['description_mini'] = $request->description_mini;
        $input['description'] = $request->description;
        $input['description_full'] = $request->description_full;
        $input['specification'] = $request->specification;
        $input['download_link'] = $request->download_link;
        $input['count'] = $request->count;
        $input['price'] = $request->price;
        $input['discount'] = $request->discount;
        $input['sold_count'] = $request->sold_count;
        $input['brand'] = $request->brand;
        $input['duration'] = $request->duration;
        $input['vote'] = $request->vote;
        $input['total_voters'] = $request->total_voters;
        $input['image_main'] = $request->image_main;
        $input['image_two'] = $request->image_two;
        $input['image_three'] = $request->image_three;
        $input['image_four'] = $request->image_four;
        $input['image_five'] = $request->image_five;

        $input['real_price'] = $input['price'] - $input['discount'];

        if (isset($request->is_active))
            $input['is_active'] = $request->is_active;

//        $product = $this->productRepository->update($request->all(), $id);
        Product::where('id', $id)->update($input);


        $product = Product::find($id);

        $product->tags()->sync($request->tag_id);

        $product->categories()->sync($request->category_id);

        Flash::success('Product updated successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }
}
