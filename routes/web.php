<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index.index');
Route::get('/category/{category}', 'IndexController@filter')->name('filter');

Route::get('/profile', 'IndexController@profileGet')->name('profile.get');
Route::get('/profile/edit', 'IndexController@profileEdit')->name('profile.edit');
Route::post('/profile/post', 'IndexController@profilePost')->name('profile.post');

Route::post('/profile/payFactor', 'IndexController@payFactor')->name('pay.factor');

Route::get('/ticketGet', 'IndexController@ticketGet')->name('ticket.get');
Route::post('/ticketPost', 'IndexController@ticketPost')->name('ticket.post');
Route::get('/answerTicket/{ticket}', 'TicketController@answerTicket')->name('answer.ticket');
Route::post('/answerTicket', 'TicketController@answerTicketPost')->name('answer.ticket.post');

Route::get('post/{post}', 'IndexController@showAPost');

Route::get('upload', 'UploadController@uploadGet')->name('upload.get');
Route::post('upload', 'UploadController@uploadPost')->name('upload.post');

Route::post('addToBasket/{id}', 'IndexController@addToBasket')->name('add.to.basket');
Route::post('removeFromBasket/{id}', 'IndexController@removeFromBasket')->name('remove.from.basket');

Route::get('checkOut', 'IndexController@checkOut')->name('check.out');

Route::get('shoppingCart', 'IndexController@shoppingCart')->name('shopping.cart');

Route::get('loginOrRegister', 'IndexController@loginOrRegister')->name('login.or.register');

Route::post('commentStoreSec', 'CommentController@storesec')->name('comment.store.sec');

Route::post('profile/increaseBalance', 'IndexController@increaseBalancePost')->name('increase.balance.post');
Route::get('profile/increaseBalance', 'IndexController@increaseBalanceGet')->name('increase.balance.get');

Route::get('profile/show/', 'IndexController@profile')->name('user.profile');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('products', 'ProductController');
Route::resource('users', 'UserController');
Route::resource('factors', 'FactorController');
Route::resource('categories', 'CategoryController');
Route::resource('tags', 'TagController');
Route::resource('comments', 'CommentController');
Route::resource('posts', 'PostController');
Route::resource('tickets', 'TicketController');

Route::get('{product}', 'IndexController@showProduct')->name('show.product.page');


