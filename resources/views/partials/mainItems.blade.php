<div class="section-home-products">
    <div class="container">
        <div class="controls-holder nav-tabs">
            <ul class="inline">
                <li class="
                @if(isset($s))
                    @if($s == 'sold_count')
                        active
                    @endif
                @endif
                        ">
{{--                    <a target="_self" href="{{ route('index.index') }}">پرفروش ترین ها</a>--}}
                    <a target="_self" href="?sort_by=sold_count">پرفروش ترین ها</a>
                </li>
                <li class="
                @if(isset($s))
                    @if($s == 'updated_at')
                        active
                    @endif
                @endif
                        ">
{{--                    <a target="_self" href="{{ route('index.new') }}">محصولات جدید</a>--}}
                    <a target="_self" href="?sort_by=updated_at">محصولات جدید</a>
                </li>
                <li class="
                @if(isset($s))
                    @if($s == 'discount')
                        active
                    @endif
                @endif
                        ">
{{--                    <a target="_self" href="{{ route('index.discount') }}">محصولات تخفیف دار</a>--}}
                    <a target="_self" href="?sort_by=discount">محصولات تخفیف دار</a>
                </li>
                <li class="
                @if(isset($s))
                @if($s == 'price')
                        active
                    @endif
                @endif
                        ">
                    {{--                    <a target="_self" href="{{ route('index.index') }}">پرفروش ترین ها</a>--}}
                    <a target="_self" href="?sort_by=price">قیمت</a>
                </li>
            </ul>
        </div>
        <div class="tab-content">

            <div id="hot-products" class="products-holder active tab-pane ">
                @php
                    $i = 0;
                @endphp
                @foreach($products as $product)
                    @php
                        if ($i % 4 == 0){
                        echo '<div class="row-fluid">';
                        }
                    @endphp
                    <div class="span3">
                        <div class="product-item">
                            <a href="{{ route('show.product.page', $product->id) }}">
                                <img alt="" src="{{ $product->image_main }}" width="200px" height="200px"/>
                                @if(isset($product->brand))
                                    <h1>{{ $product->brand }}</h1>
                                @endif
                            </a>
                            <div class="tag-line">
                                <span style="font-size: large"><strong>{{ $product->title }}</strong></span>
                                <span>{{ $product->description_mini }}</span>
                            </div>
                            @if($product->discount > 0)
                                <div
                                        style="
                                color: #FFBB00;
                                text-decoration: line-through;
                                font-size: 14px;
                                margin-left: 10px;
                                font-weight: normal;
                                ">
                                    {{ $product->price.' تومان ' }}
                                </div>
                            @endif
                            <div class="price">
                                {{ ($product->price-$product->discount) .' تومان ' }}
                            </div>
                            {!! Form::open(['route'=>['add.to.basket', $product->id ], 'method' => 'post' ]) !!}

                            <button type="submit" class="cusmo-btn add-button">افزودن به سبد خرید</button>

                            {!! Form::close() !!}
                        </div>
                    </div>
                    @php
                        if ($i % 4 == 3){
                        echo '</div>';
                        }
                        $i++;
                    @endphp
                @endforeach

                @php
                    if ($i % 4 != 4){
                    echo '</div>';
                    }
                @endphp
                <div class="row">
                    <div class="text-center">
                        {{ $products->appends(Request::capture()->except('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

                {{--<div class="span3">--}}
                {{--<div class="product-item">--}}

                {{--<a href="products-page.html">--}}
                {{--<img alt="" src="images/p1.jpg"/>--}}
                {{--<h1>versace</h1>--}}
                {{--</a>--}}
                {{--<div class="tag-line">--}}
                {{--<span>yellow diamond</span>--}}
                {{--<span>toilet water spray</span>--}}
                {{--</div>--}}
                {{--<div class="price">--}}
                {{--$270.00--}}
                {{--</div>--}}
                {{--<a class="cusmo-btn add-button" href="#">add to cart</a>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="span3">--}}
                    {{--<div class="product-item">--}}
                        {{--<a href="products-page.html">--}}
                            {{--<img alt="" src="images/p2.jpg"/>--}}
                            {{--<h1>estee lauder</h1>--}}
                        {{--</a>--}}
                        {{--<div class="tag-line">--}}
                            {{--<span>yellow diamond</span>--}}
                            {{--<span>toilet water spray</span>--}}
                        {{--</div>--}}
                        {{--<div class="price">--}}
                            {{--$122.00--}}
                        {{--</div>--}}
                        {{--<a class="cusmo-btn add-button" href="#">add to cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="span3">--}}
                    {{--<div class="product-item">--}}
                        {{--<a href="products-page.html">--}}
                            {{--<img alt="" src="images/p3.jpg"/>--}}
                            {{--<h1>burberry</h1>--}}
                        {{--</a>--}}
                        {{--<div class="tag-line">--}}
                            {{--<span>yellow diamond</span>--}}
                            {{--<span>toilet water spray</span>--}}
                        {{--</div>--}}
                        {{--<div class="price">--}}
                            {{--$120.00--}}
                        {{--</div>--}}
                        {{--<a class="cusmo-btn add-button" href="#">add to cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="span3">--}}
                    {{--<div class="product-item">--}}
                        {{--<a href="products-page.html">--}}
                            {{--<img alt="" src="images/p4.jpg"/>--}}
                            {{--<h1>versace</h1>--}}
                        {{--</a>--}}
                        {{--<div class="tag-line">--}}
                            {{--<span>yellow diamond</span>--}}
                            {{--<span>toilet water spray</span>--}}
                        {{--</div>--}}
                        {{--<div class="price">--}}
                            {{--$20.00--}}
                        {{--</div>--}}
                        {{--<a class="cusmo-btn add-button" href="#">add to cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row-fluid">--}}
                {{--<div class="span3">--}}
                    {{--<div class="product-item">--}}
                        {{--<a href="products-page.html">--}}
                            {{--<img alt="" src="images/p5.jpg"/>--}}
                            {{--<h1>versace</h1>--}}
                        {{--</a>--}}
                        {{--<div class="tag-line">--}}
                            {{--<span>yellow diamond</span>--}}
                            {{--<span>toilet water spray</span>--}}
                        {{--</div>--}}
                        {{--<div class="price">--}}
                            {{--$270.00--}}
                        {{--</div>--}}
                        {{--<a class="cusmo-btn add-button" href="#">add to cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="span3">--}}
                    {{--<div class="product-item">--}}
                        {{--<a href="products-page.html">--}}
                            {{--<img alt="" src="images/p6.jpg"/>--}}
                            {{--<h1>estee lauder</h1>--}}
                        {{--</a>--}}
                        {{--<div class="tag-line">--}}
                            {{--<span>yellow diamond</span>--}}
                            {{--<span>toilet water spray</span>--}}
                        {{--</div>--}}
                        {{--<div class="price">--}}
                            {{--$122.00--}}
                        {{--</div>--}}
                        {{--<a class="cusmo-btn add-button" href="#">add to cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="span3">--}}
                    {{--<div class="product-item">--}}
                        {{--<a href="products-page.html">--}}
                            {{--<img alt="" src="images/p7.jpg"/>--}}
                            {{--<h1>burberry</h1>--}}
                        {{--</a>--}}
                        {{--<div class="tag-line">--}}
                            {{--<span>yellow diamond</span>--}}
                            {{--<span>toilet water spray</span>--}}
                        {{--</div>--}}
                        {{--<div class="price">--}}
                            {{--$120.00--}}
                        {{--</div>--}}
                        {{--<a class="cusmo-btn add-button" href="#">add to cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="span3">--}}
                    {{--<div class="product-item">--}}
                        {{--<a href="products-page.html">--}}
                            {{--<img alt="" src="images/p8.jpg"/>--}}
                            {{--<h1>versace</h1>--}}
                        {{--</a>--}}
                        {{--<div class="tag-line">--}}
                            {{--<span>yellow diamond</span>--}}
                            {{--<span>toilet water spray</span>--}}
                        {{--</div>--}}
                        {{--<div class="price">--}}
                            {{--$20.00--}}
                        {{--</div>--}}
                        {{--<a class="cusmo-btn add-button" href="#">add to cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}

            {{--<div class="load-more-holder">--}}
                {{--<a href="#hot-products" class="load-more">--}}
                    {{--load more hot products--}}
                {{--</a>--}}
            {{--</div>--}}





            {{--<div id="new-products" class="products-holder  tab-pane ">--}}
            {{--@php--}}
            {{--$i = 0;--}}
            {{--@endphp--}}
            {{--@foreach($products_new as $product)--}}
            {{--@php--}}
            {{--if ($i % 4 == 0){--}}
            {{--echo '<div class="row-fluid">';--}}
            {{--}--}}
            {{--@endphp--}}
            {{--<div class="span3">--}}
            {{--<div class="product-item">--}}
            {{--<a href="{{ route('show.product.page', $product->id) }}">--}}
            {{--<img alt="" src="{{ $product->image_main }}" width="200px" height="200px"/>--}}
            {{--@if(isset($product->brand))--}}
            {{--<h1>{{ $product->brand }}</h1>--}}
            {{--@endif--}}
            {{--</a>--}}
            {{--<div class="tag-line">--}}
            {{--<span style="font-size: large"><strong>{{ $product->title }}</strong></span>--}}
            {{--<span>{{ $product->description_mini }}</span>--}}
            {{--</div>--}}
            {{--@if($product->discount > 0)--}}
            {{--<div--}}
            {{--style="--}}
            {{--color: #FFBB00;--}}
            {{--text-decoration: line-through;--}}
            {{--font-size: 14px;--}}
            {{--margin-left: 10px;--}}
            {{--font-weight: normal;--}}
            {{--">--}}
            {{--{{ $product->price.' تومان ' }}--}}
            {{--</div>--}}
            {{--@endif--}}
            {{--<div class="price">--}}
            {{--{{ ($product->price-$product->discount) .' تومان ' }}--}}
            {{--</div>--}}
            {{--{!! Form::open(['route'=>['add.to.basket', $product->id ], 'method' => 'post' ]) !!}--}}

            {{--<button type="submit" class="cusmo-btn add-button">افزودن به سبد خرید</button>--}}

            {{--{!! Form::close() !!}--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@php--}}
            {{--if ($i % 4 == 3){--}}
            {{--echo '</div>';--}}
            {{--}--}}
            {{--$i++;--}}
            {{--@endphp--}}
            {{--@endforeach--}}
            {{--<div class="row">--}}
            {{--<div class="text-center">--}}
            {{--{{ $products_new->links() }}--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@php--}}
            {{--if ($i % 4 != 4){--}}
            {{--echo '</div>';--}}
            {{--}--}}
            {{--@endphp--}}
            {{--</div>--}}


            {{--<div id="best-sellers" class="products-holder  tab-pane ">--}}
            {{--@php--}}
            {{--$i = 0;--}}
            {{--@endphp--}}
            {{--@foreach($products_discount as $product)--}}
            {{--@php--}}
            {{--if ($i % 4 == 0){--}}
            {{--echo '<div class="row-fluid">';--}}
            {{--}--}}
            {{--@endphp--}}
            {{--<div class="span3">--}}
            {{--<div class="product-item">--}}

            {{--<a href="{{ route('show.product.page', $product->id) }}">--}}
            {{--<img alt="" src="{{ $product->image_main }}" width="200px" height="200px"/>--}}
            {{--@if(isset($product->brand))--}}
            {{--<h1>{{ $product->brand }}</h1>--}}
            {{--@endif--}}
            {{--</a>--}}
            {{--<div class="tag-line">--}}
            {{--<span style="font-size: large"><strong>{{ $product->title }}</strong></span>--}}
            {{--<span>{{ $product->description_mini }}</span>--}}
            {{--</div>--}}
            {{--@if($product->discount > 0)--}}
            {{--<div--}}
            {{--style="--}}
            {{--color: #FFBB00;--}}
            {{--text-decoration: line-through;--}}
            {{--font-size: 14px;--}}
            {{--margin-left: 10px;--}}
            {{--font-weight: normal;--}}
            {{--">--}}
            {{--{{ $product->price.' تومان ' }}--}}
            {{--</div>--}}
            {{--@endif--}}
            {{--<div class="price">--}}
            {{--{{ ($product->price-$product->discount) .' تومان ' }}--}}
            {{--</div>--}}

            {{--{!! Form::open(['route'=>['add.to.basket', $product->id ], 'method' => 'post' ]) !!}--}}

            {{--<button type="submit" class="cusmo-btn add-button">افزودن به سبد خرید</button>--}}

            {{--{!! Form::close() !!}--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@php--}}
            {{--if ($i % 4 == 3){--}}
            {{--echo '</div>';--}}
            {{--}--}}
            {{--$i++;--}}
            {{--@endphp--}}
            {{--@endforeach--}}
            {{--<div class="row">--}}
            {{--<div class="text-center">--}}
            {{--{{ $products_discount->links() }}--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@php--}}
            {{--if ($i % 4 != 4){--}}
            {{--echo '</div>';--}}
            {{--}--}}
            {{--@endphp--}}
            {{--</div>--}}


