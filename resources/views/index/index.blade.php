@extends('layouts.index')
@section('header')
    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    {{--    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">--}}
    <link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
@endsection
@section('content')

    <div class="wrapper">

        @include('partials.header')


        @include('partials.mainSlider')


        @include('partials.mainItems')


{{--        @include('partials.footer')--}}


        @include('partials.copyright')


    </div>


@endsection
@push('scripts')
    <script>
        $('.pagination li').addClass('page-item');
        $('.pagination li a').addClass('page-link').addClass('btn btn-primary');
        $('.pagination span').addClass('page-link').addClass('btn btn-primary');
    </script>
@endpush