<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Models\Category;
use App\Models\Factor;
use App\Models\Post;
use App\Models\Product;
use App\Models\Ticket;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Session;

class IndexController extends Controller
{

    public function index(Request $request)
    {
////        return redirect(route('filter', 'همه محصولات'));
//        if (!isset($request)) {
//            $s = 'sold_count';
//        } else {
//            $s = $request->sort_by;
//        }
//        dd($request);
//
//        $c = 'همه محصولات';
//        $products = Product::orderByDesc($s)->paginate(20);
////        $this->filter('hi', 'همه محصولات');
////
//        if ($s == 'price')
//            $products = Product::orderBy($s)->paginate(20);
//        $request = new Request();
        $total = $this->filter( Request(),  null);

        $products = $total->products;
        $s = $total->s;
        $c = $total->c;

        return view('index.index', compact('products', 's', 'c'));
//        return view('index.index', compact('products', 's', 'c'));
    }

    public function filter(Request $request, $category)
    {
        //$s is active class for sorting and $c is active class for category.

        if (!isset($request->sort_by)) {
            $s = 'sold_count';
        }else{
            $s = $request->sort_by;
        }

        if (!($s == 'price')) {

            if (isset($category)) {
                $cat = Category::where('name', $category)->first();
                $c = $cat->name;
                $products = $cat->products()->orderByDesc($s)->paginate(20);
            } else {
                $c = 'همه محصولات';
                $products = Product::orderByDesc($s)->paginate(20);
            }

        } else {

            if (isset($category)) {
                $cat = Category::where('name', $category)->first();
                $c = $cat->name;
                $products = $cat->products()->orderBy('real_price')->paginate(20);
            } else {
                $c = 'همه محصولات';
                $products = Product::orderBy('real_price')->paginate(20);
            }

        }


        return view('index.index', compact('products', 's', 'c'));
    }

    public function profileGet()
    {
        $user = User::find(auth()->user()->id);
        $factors = Factor::where('user_id', auth()->user()->id)->with('products')->get();

        return view('index.usersProfile', compact('user', 'factors'));
    }

    public function profileEdit()
    {
        $user = User::find(auth()->user()->id);

        return view('index.usersProfileEdit', compact('user'));
    }

    public function profilePost(CreateUserRequest $request)
    {
        if (isset($request->name))
            $inputs['name'] = $request->name;

        if (isset($request->email))
            $inputs['email'] = $request->email;


        if ($request->password == $request->password_retype) {
            if (isset($request->password))
                $inputs['password'] = bcrypt($request->password);

            $user = User::find(auth()->user()->id);
            $user->update($inputs);

            return redirect(route('profile.get'));
        } else {
            Flash::error('رمز عبور و تکرار رمز عبور مطابقت ندارند دقت کنید که هر دو را مشابه هم وارد کنید.');

            return redirect()->back();
        }
    }

    public function ticketGet()
    {
        $user = User::find(auth()->user()->id);
        $firstTicket = $user->tickets()->first();

        if (count((array)$firstTicket)) {
            $id = auth()->user()->id;
            $user = User::find($id);
            $firstTicket = $user->tickets()->first();

            $tickets = Ticket::where('parent_id', $firstTicket->id)->get();
        }

        return view('index.showTickets', compact('firstTicket', 'tickets'));
    }

    public function ticketPost(Request $request)
    {
        $inputs['user_id'] = auth()->user()->id;
        $inputs['email'] = auth()->user()->email;
        $inputs['who_answer'] = auth()->user()->name;
        $inputs['text'] = $request->text;
        $inputs['is_read'] = 0;
        $inputs['closed'] = 0;

        $user = User::find(auth()->user()->id);
        $firstTicket = $user->tickets()->first();

        if (count((array)$firstTicket)) {
            $inputs['parent_id'] = $firstTicket->id;
        }

        Ticket::create($inputs);
        Flash::success('پیغام شما ثبت شد. همکاران ما به زودی به پیغام شما پاسخ خواهند داد.');

        return redirect()->back();
    }

    public function showAPost(Post $post)
    {
        return view('index.showAPost', compact('post'));
    }

    public function increaseBalanceGet()
    {
        return view('index.increaseBalance');
    }

    public function increaseBalancePost(Request $request)
    {
        dd($request);
        //must be complete...

        Flash::success('مبلغ حساب شما افزایش پیدا کرد.');

        return view('index.index');
    }

    public function showProduct(Product $product)
    {
        $products = Product::orderByDesc('sold_count')->get()->take(10);
        $comments = $product->comments()->orderByDesc('created_at')->get();

        return view('index.showProduct', compact('product', 'comments', 'products'));
    }

    public function addToBasket($id)
    {
        $arr = Session::get('itemId');
        $arr[] = $id;
        Session::put('itemId', $arr);

        return redirect()->back();
    }

    public function removeFromBasket($id)
    {
        $arr = Session::get('itemId');
        $arr = array_unique($arr);
        if (count($arr) > 1) {
            if ($key = array_search($id, $arr) !== false) {
                $key = array_search($id, $arr);
                unset($arr[$key]);
                Session::forget('itemId');
                Session::put('itemId', $arr);
            }
        } else {
            Session::forget('itemId');
        }

        return redirect()->back();
    }

    public function shoppingCart()
    {
        $products = '';
        $id = session()->get('itemId');
        if (isset($id)) {
            $whereData = ['myselect' => $id];
            $query = '';
            foreach ($whereData['myselect'] as $select) {
                $query .= " id = $select or";
            }
            $query = 'select * from products where' . $query . ' id = 0';
            $products = DB::select(DB::raw($query));
        }

        return view('index.shoppingCart', compact('products'));
    }

    public function checkOut()
    {
        if (! Auth::check())
            return view('index.loginRegister');

        $products = '';
        $id = session()->get('itemId');
//        dd($id);
        if (isset($id)) {
            $whereData = ['myselect' => $id];
            $query = '';
            foreach ($whereData['myselect'] as $select) {
                $query .= " id = $select or";
            }
            $query = 'select * from products where' . $query . ' id = 0';
            $products = DB::select(DB::raw($query));
        }
        $sum = 0;
        $discount = 0;

        foreach($products as $product){
            $sum += $product->price ;
            $discount += $product->discount;
        }

        Factor::create([
            'user_id'      => auth()->user()->id,
            'sum'          => $sum,
            'discount_sum' => $discount,
            'is_paid'      => 0,
        ]);

        $factor = DB::table('factors')->orderBy('id', 'desc')->first();

        foreach($products as $product){
            DB::table('factor_product')->insert(['factor_id' => $factor->id, 'product_id' => $product->id]);
        }

        Session::forget('itemId');

        return view('index.checkOut', compact('products', 'sum', 'discount'));
    }

    public function loginOrRegister()
    {
        if ( Auth::check())
            return redirect(route('index.index'));

        return view('index.loginRegister');
    }


}
