<style>
    span, h1 {
        text-shadow: 1px 2px 2px rgb(208, 180, 255);

    }
</style>
<section id="home" class="home-slider">
    <div class="container" >
        <div class="flexslider">
            <ul class="slides">
                <li class="slide">
                    <img alt="" src="http://shop.oo/uploads/files/sadegh/1516187478scratchmysoulx970x549.jpg" style="margin-right: 120px"/>
                    <div class="flex-caption">
                        <h1>تخفیف</h1>
                        <div class="medium">
                            <span>تخفیف ها عالی</span>
                        </div>
                        <div class="small">
                            <span>بشترین تخفیف و بهترین قیمت را با ما تجربه کنید.</span>
                        </div>
                        <div class="small yellow">
                            <span>کیفیت . قیمت</span>
                        </div>
                        <div>
                            <span><a class="cusmo-btn" href="#">بیشتر...</a></span>

                        </div>

                    </div>
                </li>
                <li class="slide">
                    <img alt="" src="http://shop.oo/uploads/files/sadegh/15161936596254047-shop-wallpaper-onlinex970x549.jpg" style="margin-right: 120px"/>
                    <div class="flex-caption">
                        <h1>پیشرفت</h1>
                        <div class="medium">
                            <span>آموزش و تجربه</span>
                        </div>
                        <div class="small">
                            <span>بهترین و کاربردی ترین محصولا آموزشی را از ما بخواهید.</span>
                        </div>
                        <div class="small yellow">
                            <span>آموزش . تجربه . خلاقیت . کیفیت</span>
                        </div>
                        <div>
                            <span><a class="cusmo-btn" href="#">بیشتر...</a></span>

                        </div>

                    </div>
                </li>


            </ul>
        </div>
    </div>
</section>