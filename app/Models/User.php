<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class User
 * @package App\Models
 * @version January 15, 2018, 6:07 pm UTC
 *
 * @property string name
 * @property string email
 * @property string password
 * @property integer role_id
 * @property integer balance
 */
class User extends Model
{

    public $table = 'users';
    


    public $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'balance'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'role_id' => 'integer',
        'balance' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];



    
}
