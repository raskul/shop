<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Ticket
 * @package App\Models
 * @version January 22, 2018, 12:37 pm UTC
 *
 * @property string username
 * @property string email
 * @property string text
 */
class Ticket extends Model
{

    public $table = 'tickets';
    


    public $fillable = [
        'user_id',
        'email',
        'text',
        'closed',
        'is_read',
        'parent_id',
        'who_answer'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'username' => 'string',
        'email' => 'string',
        'text' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
