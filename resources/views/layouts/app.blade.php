<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>فروشگاه فایل</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/css/AdminLTE.min.css">--}}
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/css/skins/_all-skins.min.css">--}}

    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("bower_components/font-awesome/css/font-awesome.min.css") }}">
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
    <link rel="stylesheet" href="{{ asset("bower_components/adminlte-rtl/dist/css/AdminLTE-rtl.css") }}">
    <link rel="stylesheet" href="{{ asset("bower_components/adminlte-rtl/dist/css/skins/_all-skins-rtl.min.css") }}">

    <link rel="stylesheet" href="{{ asset('bower_components/pwt.datepicker/dist/css/persian-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-table/dist/bootstrap-table.min.css') }}">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('iransans/css/fontiran.css') }}">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>

    @stack('header')


    <style>
        body {
            font-family: IRANSans !important;
            font-weight: 300;
            direction: rtl;
            background-color: #E2E2E2;
            margin: 0;
        }
        h1, h2, h3, h4, h5, h6,input, textarea {
            font-family: IRANSans !important;
        }
        .datepicker-plot-area {
            position: relative;
        }
        .tooltip{
            font-family: IRANSans, Tahoma;
        }
    </style>
</head>

<body class="skin-blue sidebar-mini">
@stack('modal')
@if (!Auth::guest())
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="#" class="logo">
                <b>فروشگاه فایل</b>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="{{ asset('images/user.png') }}"
                                     class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">
                                      @if (Auth::guest())
                                        InfyOm
                                    @else
                                        {!! Auth::user()->name !!}
                                    @endif
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="{{ asset('images/user.png') }}"
                                         class="img-circle" alt="User Image"/>
                                    <p>
                                        @if (Auth::guest())
                                            فروشگاه فایل
                                        @else
                                            {!! Auth::user()->name !!}
                                        @endif
                                        <small>Member since {!! Auth::user()->created_at->format('M. Y') !!}</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        {{--<a href="{{ route('users.edit', auth()->user()->id) }}" class="btn btn-default btn-flat">پروفایل</a>--}}
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">خروج از حساب</a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
    @include('layouts.sidebar')
    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: center">
            <strong>© 2018 <a target="_blank" href="#">فروشگاه</a>.</strong> تمامی حقوق برای فروشگاه محفوظ است.
        </footer>

    </div>
@else
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{!! url('/') !!}">
                    InfyOm Generator
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{!! url('/home') !!}">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{!! url('/login') !!}">Login</a></li>
                        <li><a href="{!! url('/register') !!}">Register</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
@endif

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>

<!-- jQuery 2.1.4 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

<!-- AdminLTE App -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/js/app.min.js"></script>

<!-- Datatables -->
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('bower_components/persian-date/dist/persian-date.min.js') }}"></script>
<script src="{{ asset('bower_components/pwt.datepicker/dist/js/persian-datepicker.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/i18n/fa.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-table/dist/bootstrap-table.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-table/dist/locale/bootstrap-table-fa-IR.min.js') }}"></script>
<script src="{{ asset('js/wordifyfa.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="{{ asset('bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $('textarea#editorcm').ckeditor();
    });
</script>

<script>
    (function ($, window) {
        $.fn.replaceOptions = function (options) {
            var self, $option;

            this.empty();
            self = this;

            $.each(options, function (index, option) {
                $option = $("<option></option>")
                    .attr("value", index)
                    .text(option);
                self.append($option);
            });
        };

        $.fn.pressEnter = function(fn) {

            return this.each(function() {
                $(this).bind('enterPress', fn);
                $(this).keyup(function(e){
                    if(e.keyCode == 13)
                    {
                        $(this).trigger("enterPress");
                    }
                })
            });
        };
    })(jQuery, window);

    (function ($, window) {
        $.fn.formatNumbers = function () {
            this.on('keyup keypress blur change', function(){
                if($(this).val() != ''){
                    var value = $(this).val().split(',').join('');
                    $(this).val(Number(value).toLocaleString());
                }
            });
        };
    })(jQuery, window);
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var options = {
            format: 'YYYY/MM/DD',
            initialValue: true,
            navigator:{
                text:{
                    btnNextText: ">",
                    btnPrevText: "<"
                }
            }
        };
        var options_not_init = {
            format: 'YYYY/MM/DD',
            initialValue: false,
            navigator:{
                text:{
                    btnNextText: ">",
                    btnPrevText: "<"
                }
            }
        };
        $(".pdatepicker").pDatepicker(options);
        $('body').on('focusin', '.pdatepicker', function(e) {
            $(this).pDatepicker(options);
        });
        $(".pdatepicker_not_init").pDatepicker(options_not_init);
        $('body').on('focusin', '.pdatepicker_not_init', function(e) {
            $(this).pDatepicker(options_not_init);
        });
        var time_picker_options = {
            format: 'HH:mm',
            onlyTimePicker: true,
            initialValue: true
        };
        var time_picker_options_not_init = {
            format: 'HH:mm',
            onlyTimePicker: true,
            initialValue: false
        };
        $(".timepicker").pDatepicker(time_picker_options);
        $('body').on('focusin', '.timepicker', function(e) {
            $(this).pDatepicker(time_picker_options);
        });
        $(".timepicker_not_init").pDatepicker(time_picker_options_not_init);
        $('body').on('focusin', '.timepicker_not_init', function(e) {
            $(this).pDatepicker(time_picker_options_not_init);
        });
    });
</script>
<script>
    $(".select2").select2({
        dir: "rtl",
        language: "fa",
        width: '100%'
    });
</script>
<script>
    $(document).ready(function(){
        $('[title]').tooltip();
    });


    $('.format_number').formatNumbers();

    $('form').submit(function () {
        $(this).find('.format_number').each(function () {
            var value = $(this).val().split(',').join('');
            $(this).val(value);
        });
        return true;
    });
</script>
@yield('scripts')
@stack('scripts')
</body>
</html>