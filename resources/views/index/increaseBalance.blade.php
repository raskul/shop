@extends('layouts.index')
@section('header')
    {{--    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">
    {{--    <link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
@endsection
@section('content')

    {!! Form::open(['route'=>['increase.balance.post', 'id' => Auth::user()->id ], 'method' => 'post' ]) !!}

    <!-- فیلد مبلغی که میخواهید حساب شما افزایش پیدا کند را بنویسید: -->
    <div class="form-group {{ $errors->has('amount') ? ' has-error' : '' }}">
        {!! Form::label('amount', 'مبلغی که میخواهید حساب شما افزایش پیدا کند را بنویسید:') !!}
        {!! Form::text('amount', null, ['class' => 'form-control']) !!}
        @if ($errors->has('amount'))
            <span class="help-block"><strong>{{ $errors->first('amount') }}</strong></span>
        @endif
    </div>

    <button class="btn btn-success">افزاش موجودی</button>
    {!! Form::close() !!}

@endsection